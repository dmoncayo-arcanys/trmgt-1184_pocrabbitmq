﻿using Newtonsoft.Json;
using POCRabbitMQ.Contracts;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace POCRabbitMQ
{
    public class DirectExchangePublisher
    {

        private readonly IModel channel;

        public DirectExchangePublisher(IModel channel)
        {
            this.channel = channel;
            var ttl = new Dictionary<string, object>
            {
                { "x-message-ttl", 30000 }
            };
            channel.ExchangeDeclare("poc-rabbit-mq", ExchangeType.Direct, arguments: ttl);
        }


        public void Publish(string name, string message)
        {
            var post = new PostModel
            { 
                Name = name,
                Message = message,
                Timestamp = DateTime.Now,
            };

            var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(post));
            channel.BasicPublish("poc-rabbit-mq", "poc-key", null, body);
        }
    }
}
