﻿using RabbitMQ.Client;
using System;
using System.Threading;

namespace POCRabbitMQ
{
    class Program
    {
        private static readonly string[] NAMES = { "John Cena", "Randy Order", "John Doe" };
        private static readonly string[] MESSAGES = {
            "The quick brown fox jumped over the lazy dog.",
            "We are the world, we are the children.",
            "There comes a time, when we hear a certain call, when the world must come together as one."
        };

        static void Main(string[] args)
        {
            var factory = new ConnectionFactory
            {
                Uri = new Uri("amqp://guest:guest@host.docker.internal:5672")
            };
            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();

            var publish = new DirectExchangePublisher(channel);

            Console.WriteLine("---------------------");
            Console.WriteLine(" Post Publisher");
            Console.WriteLine("---------------------");

            var random = new Random();
            do
            {
                int index = random.Next(0, 3);
                var name = RandomName(index);
                var message = RandomMessage(index);

                // Send message.
                publish.Publish(name, message);

                Console.WriteLine(" Post sent..");

                Thread.Sleep(1000);
            } 
            while (true);
        }

        private static string RandomName(int index)
        {
            return NAMES[index];
        }

        private static string RandomMessage(int index)
        {
            return MESSAGES[index];
        }
    }
}
