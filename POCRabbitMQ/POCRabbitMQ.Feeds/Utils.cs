﻿using Newtonsoft.Json;
using System;
using System.IO;

namespace POCRabbitMQ.Feeds
{
    public class Utils
    {
        public static T ByteArrayToObject<T>(byte[] buffer)
        {
            using var reader = new StreamReader(new MemoryStream(buffer));
            return JsonConvert.DeserializeObject<T>(reader.ReadToEnd());
        }

        public static string FormatDateTime(DateTime timestamp)
        {
            return timestamp.ToString("dddd, dd MMMM yyyy HH:mm:ss");
        }
    }
}
