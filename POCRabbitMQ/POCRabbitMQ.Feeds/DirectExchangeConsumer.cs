﻿using POCRabbitMQ.Contracts;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;

namespace POCRabbitMQ.Feeds
{
    public class DirectExchangeConsumer
    {

        private readonly IModel channel;

        public DirectExchangeConsumer(IModel channel)
        {
            this.channel = channel;
        }

        public void Consume()
        {
            channel.ExchangeDeclare("poc-rabbit-mq", ExchangeType.Direct);
            channel.QueueDeclare("poc-rabbit-mq-queue",
                durable: true,
                exclusive: false,
                autoDelete: false,
                arguments: null);
            channel.QueueBind("poc-rabbit-mq-queue", "poc-rabbit-mq", "poc-key");
            channel.BasicQos(0, 10, false);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (sender, e) =>
            {
                var body = e.Body.ToArray();
                var post = Utils.ByteArrayToObject<PostModel>(body);

                Console.WriteLine(" Publisher: " + post.Name);
                Console.WriteLine(" Message: " + post.Message);
                Console.WriteLine(" Timestamp: " + Utils.FormatDateTime(post.Timestamp));
                Console.WriteLine("\n");
            };

            channel.BasicConsume("poc-rabbit-mq-queue", true, consumer);
            Console.ReadLine();
        }
    }
}
