﻿using RabbitMQ.Client;
using System;

namespace POCRabbitMQ.Feeds
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory
            {
                Uri = new Uri("amqp://guest:guest@host.docker.internal:5672")
            };
            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();

            Console.WriteLine("---------------------");
            Console.WriteLine(" Post Feeds");
            Console.WriteLine("---------------------\n");

            var consume = new DirectExchangeConsumer(channel);
            consume.Consume();
        }
    }
}
