﻿using System;

namespace POCRabbitMQ.Contracts
{
    public class PostModel : IPostModel
    {
        public string Name { get; set; }

        public string Message { get; set; }

        public DateTime Timestamp { get; set; }
    }
}
