﻿using System;

namespace POCRabbitMQ.Contracts
{
    public interface IPostModel
    {
        public string Name { get; set; }

        public string Message { get; set; }

        public DateTime Timestamp { get; set; }
    }
}
