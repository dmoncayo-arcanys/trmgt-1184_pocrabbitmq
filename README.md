# POC for RabbitMQ #


### How do I get set up? ###

* Go to CMD
* Run `docker run -d --hostname host-rabbitmq --name rabbitmq -p 15672:15672 -p 5672:5672 rabbitmq:3-management` to create RabbitMQ container.
* Go to <Directory>/trmgt-1184_pocrabbitmq/POCRabbitMQ/POCRabbitMQ/
* Run `docker build -t rabbitmq-sender -f DockerFile .`
* Go to <Directory>/trmgt-1184_pocrabbitmq/POCRabbitMQ/POCRabbitMQ.Feeds/
* Run `docker build -t rabbitmq-receiver -f DockerFile .`
* If `POCRabbitMQ.Contracts` is missing, import `POCRabbitMQ.Contracts.1.0.0.nupkg` found in `<Directory>/trmgt-1184_pocrabbitmq/Nuget/`

### RabbitMQ Console ###

* http://localhost:15672
* Username: guest
* Password: guest